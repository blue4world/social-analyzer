from __future__ import print_function # In python 2.7
import sys
import os
from sqlite3 import dbapi2 as sqlite3
import sql_queries as queries
import flask
from flask import Flask, request, session, g, redirect, url_for, abort, render_template, flash, make_response
from json_graph_builder import *
from json_heatmap_builder import *
from sql_query_handler import *
from html_event_list_builder import *
from query_cache import *
from db_manager import *
from wordcloud_builder import *
from main_entities_manager import *
import time

"""
CREATE INDEX `index_end_time` ON `events` (`end_time` ASC);
CREATE INDEX `index_start_time` ON `events` (`start_time` ASC);
CREATE INDEX `index_city` ON `places` (`city` ASC);
"""

"""
speed up db
end transaction; PRAGMA synchronous=OFF;
end transaction; PRAGMA page_size = 4096;
end transaction; PRAGMA ignore_check_constraints = 1;
end transaction; PRAGMA temp_store = MEMORY;
"""

app = Flask(__name__)
app.debug = True

# Load default config and override config from an environment variable
app.config.update(dict(
    DATABASE=os.path.join(app.root_path, 'web.db'),
    DATABASE_DATA=os.path.join('/storage/social-backend', 'database.db'),
    #DATABASE_DATA=os.path.join('/home/blue4world/Desktop', 'database.db'),
    DEBUG=True,
    SECRET_KEY='development key',
    USERNAME='admin',
    PASSWORD='admin'
))
app.config.from_envvar('FLASKR_SETTINGS', silent=True)
DbManager.init(app)

@app.teardown_appcontext
def closeAllDb(error):
    DbManager.closeAllDb(error)

def printConsole(string):
    print(string, file=sys.stderr)

def getGraphType(task):
    if task == "most_visited_places":
        return "pie"
    elif task == "most_visited_events":
        return "bar"
    else:
        Exception("No graph type for: " + task)

def getSqlByTask(task=''):
    sqlBuilder = SqlQueryHandler(task, request)
    sql = sqlBuilder.getSql()
    printConsole(sql)
    return sql

def getBuilderByTask(sql,task=''):
    builder = None
    if task == "heatmap":
        builder = JsonHeatmapBuilder(sql=sql)
    elif task == "event_list":
        builder = HtmlEventListBuilder(sql=sql)
    elif task == "wordcloud_events":
        builder = WordcloudBuilder(sql=sql)
    else:
        builder = JsonGraphBuilder(getGraphType(task), sql=sql)
    return builder


@app.route('/')
@app.route('/home')
def show_entries():
    return render_template('page_home.html', main_menu_entries=getMainMenuEntries(),header_text=getHeaderText())

@app.route('/popular')
def show_popular():
    return render_template('page_graph.html', main_menu_entries=getMainMenuEntries(),header_text=getHeaderText(),graph_entities=getGraphEntities())

@app.route('/filter')
def show_filter():
    return render_template('page_filter.html', main_menu_entries=getMainMenuEntries(), header_text=getCustomHeaderText("Filter settings"),city_list=getCityList())

@app.route('/heatmap')
def show_heatmap():
    return render_template('page_heatmap.html', main_menu_entries=getMainMenuEntries(),header_text=getHeaderText())

@app.route('/event_list')
def show_event_list():
    return render_template('page_event_list.html', main_menu_entries=getMainMenuEntries(),header_text=getHeaderText())

@app.route('/wordcloud')
def show_wordcloud():
    return render_template('page_wordcloud.html', main_menu_entries=getMainMenuEntries(),header_text=getHeaderText())

@app.route('/people_search')
def show_people_search():
    return render_template('page_people_search.html', main_menu_entries=getMainMenuEntries(),header_text=getHeaderText())




@app.route('/json/<task>')
@app.route('/html/<task>')
@app.route('/img/<task>')
def getJson(task=''):
    printConsole("Request "+task)
    start_time = time.time()

    sql = getSqlByTask(task)
    QueryCache.disableCache()

    data = QueryCache.loadFromCache(sql,task)
    if data is None:
        builder = getBuilderByTask(sql,task)
        data = builder.getData()
        QueryCache.saveToCache(sql,data,task)

    timeDiff = time.time() - start_time
    printConsole("Done in " + str(timeDiff))
    start_time = time.time()

    response = make_response(data)
    response.headers['Access-Control-Allow-Origin'] = "*"
    return response















