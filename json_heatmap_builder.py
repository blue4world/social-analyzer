import main as main
import flask
from db_manager import *


class JsonHeatmapBuilder:
    def __init__(self,sql=None):
        db = DbManager.getDataDb()
        cursor = db.execute(sql)
        self.json = self.processData(cursor)

    def processData(self,cursor):
        json = { }
        json["data"] = [ ]
        for row in cursor.fetchall():
            location = { }
            location['lat'] = row[0]
            location['lng'] = row[1]
            location['count'] = row[2]
            json["data"].append(location)
        return json

    def getData(self):
        return flask.jsonify(self.json)
