import os.path
import main as main
import hashlib
import pickle
from flask import send_file

class QueryCache:
    cache_path = '/tmp/cache_'
    disable = False

    @staticmethod
    def disableCache():
        QueryCache.disable = True

    @staticmethod
    def saveToCache(sql,data,task):
        if task != "wordcloud_events":
            pickle.dump(data, open(QueryCache.getFilePath(sql), "wb"))

    @staticmethod
    def loadFromCache(sql,task):
        data = None
        if not QueryCache.disable:
            if os.path.isfile(QueryCache.getFilePath(sql)):
                data = pickle.load(open(QueryCache.getFilePath(sql), "rb"))
            if os.path.isfile(QueryCache.getFilePath(sql,extension=".png")):
                data = send_file(QueryCache.getFilePath(sql, extension=".png"))
        return data

    @staticmethod
    def getFilePath(sql,extension = ""):
        return QueryCache.cache_path+QueryCache.getFileName(sql)+extension

    @staticmethod
    def getFileName(sql):
        m = hashlib.md5()
        m.update(sql)
        return m.hexdigest()

