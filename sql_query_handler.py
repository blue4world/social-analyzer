import sql_queries as queries
import datetime
import time
from sql_queries import *
import urllib

class SqlQueryHandler:
    def __init__(self,page,request):
        self.page = page
        self.sql = ""

        city = self.getValueFromRequest(request,'city')
        start_date = self.getDateFromRequest(request,'start_date')
        stop_date = self.getDateFromRequest(request, 'stop_date')
        limit = self.getValueFromRequest(request,'limit')

        # default limits
        if limit is None: limit = 20
        limitEventList = 300
        limitWordcloudEvents = 5000


        if page == "most_visited_places":
            self.sql = queryMostVisitedPlaces(city,start_date,stop_date,limit)

        elif page == "most_visited_events":
            self.sql = queryMostVisitedEvents(city, start_date, stop_date, limit)
        elif page == "heatmap":
            self.sql = queryHeatmap(city ,start_date, stop_date)
        elif page == "event_list":
            self.sql = queryEventsList(city ,start_date, stop_date, limitEventList)
        elif page == "wordcloud_events":
            self.sql = queryWordcloudEvents(city, start_date, stop_date, limitWordcloudEvents)
        else:
            Exception("Unknown page. Unable to build SQL query for page "+page)


    def getSql(self):
        return self.sql

    def getDateFromRequest(self,request,dateType = ""):
        if dateType in request.args:
            date = request.args.get(dateType)
            if date != '':
                out = datetime.datetime.strptime(date, '%d-%m-%Y').date()
                return str(int(time.mktime(out.timetuple())))
            else:
                return None
        else:
            return None


    def getValueFromRequest(self,request,valueName):
        if valueName in request.args:
            data = request.args.get(valueName)
            if data != '':
                return urllib.unquote(data).encode('utf-8').strip()
            else:
                return None
        else:
            return None




