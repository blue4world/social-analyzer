from db_manager import *

def getMainMenuEntries():
    cur = DbManager.getDb().execute('select * from main_menu order by id asc')
    return cur.fetchall()

def getGraphEntities():
    section = request.path.replace("/", "");
    cur = DbManager.getDb().execute('select * from graphs where section = ? order by "order" asc ',(section,))
    return cur.fetchall()

def getHeaderText():
    url = request.path.replace("/", "");
    if url == "":
        url = "home"
    cur = DbManager.getDb().execute('select text from main_menu where link == ?',(url,))
    return cur.fetchone()

def getCityList():
    cur = DbManager.getDataDb().execute("SELECT city,count(city) as occurence from places group by city order by occurence desc limit 10")
    return cur.fetchall()

def getCustomHeaderText(string):
    text = { }
    text['text'] = string
    return text