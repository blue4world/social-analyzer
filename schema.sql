drop table if exists entries;
create table main_menu (
  id integer primary key autoincrement,
  'menu_text' text not null
  'menu_link' text not null
  'menu_icon' text not null
);

create table graphs (
  id integer primary key autoincrement,
  'div' text not null
  'callback' text not null
  'section' text not null
);
