from wordcloud import STOPWORDS

class WordcloudStopWordsList:
    @staticmethod
    def getWords():
        stopwords = set(STOPWORDS)
        stopwords.add("na")
        stopwords.add("amp")
        stopwords.add("quot")
        stopwords.add("ve")
        stopwords.add("pro")
        stopwords.add("vol")
        stopwords.add("se")
        stopwords.add("pod")
        stopwords.add("vs")
        stopwords.add("si")
        stopwords.add("La")
        stopwords.add("vs")
        stopwords.add("dj")
        stopwords.add("je")
        stopwords.add("bez")
        stopwords.add("de")
        stopwords.add("od")
        stopwords.add("=")
        stopwords.add("po")
        stopwords.add("aneb")
        stopwords.add("II")
        stopwords.add("ll")
        stopwords.add("za")
        stopwords.add("ne")
        stopwords.add("MC")
        stopwords.add("pa")
        stopwords.add("lo")
        stopwords.add("kc")
        stopwords.add("az")
        stopwords.add("tj")
        return stopwords
        