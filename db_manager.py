from flask import Flask, request, session, g, redirect, url_for, abort, render_template, flash
from sqlite3 import dbapi2 as sqlite3

class DbManager:
    app = None

    @staticmethod
    def init(app):
        DbManager.app = app

    @staticmethod
    def connectDb():
        rv = sqlite3.connect(DbManager.getApp().config['DATABASE'])
        rv.row_factory = sqlite3.Row
        return rv

    @staticmethod
    def connectDataDb():
        rv = sqlite3.connect(DbManager.getApp().config['DATABASE_DATA'])
        rv.row_factory = sqlite3.Row
        return rv

    @staticmethod
    def getDb():
        if not hasattr(g, 'sqlite_db'):
            g.sqlite_db = DbManager.connectDb()
        return g.sqlite_db

    @staticmethod
    def getDataDb():
        if not hasattr(g, 'sqlite_db_data'):
            g.sqlite_db_data = DbManager.connectDataDb()
        return g.sqlite_db_data

    @staticmethod
    def closeAllDb(error):
        if hasattr(g, 'sqlite_db'):
            g.sqlite_db.close()
        if hasattr(g, 'sqlite_db_data'):
            g.sqlite_db_data.close()

    @staticmethod
    def getApp():
        return DbManager.app
