var userLocalStorage = {
    getCity : function() {
       var city = localStorage.getItem('city', city);
       if (city == null){
            city = "All";
       }
       return city
    },
    getstartDate : function() {
       var start_date = localStorage.getItem('start_date', start_date);
       if (start_date == null || start_date == "undefined"){
            start_date = "1-1-2016"
       }
       return start_date
    },
    getstopDate : function() {
       var stop_date = localStorage.getItem('stop_date', stop_date);
       if (stop_date == null || stop_date == "undefined"){
            stop_date = "31-12-2016"
       }
       return stop_date
    },
    getLimit : function() {
       var limit = localStorage.getItem('limit_slider', limit);
       if (limit == null){
            limit = 20
       }
       return limit
    },
    getApiParams : function() {
       return encodeURI('city='+this.getCity()+'&limit='+this.getLimit()+'&start_date='+this.getstartDate()+'&stop_date='+this.getstopDate());
    }
};

