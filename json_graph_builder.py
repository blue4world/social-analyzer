from flask import Flask, request, session, g
import main as main
from db_manager import *
import flask

class JsonGraphBuilder:
    def __init__(self,graph_type,sql=None):
        self.graph_type = graph_type
        self.json = None
        self.initJson()

        if sql != None:
            db = DbManager.getDataDb()
            cursor = db.execute(sql)
            self.processData(cursor)


    def processData(self,cursor):
        for row in cursor.fetchall():
            self.addLabel(row[0])
            self.addValue(row[1])

    def initJson(self):
        if self.graph_type == "pie":
            self.json = JsonGraphBuilder.getNewJsonTypePie()
        elif self.graph_type == "bar":
            self.json = JsonGraphBuilder.getNewJsonTypeBar()
        else:
            Exception("Unknown graph type")


    def addLabel(self,newLabel):
        if self.graph_type == "pie":
            self.json["labels"].append(newLabel)
        elif self.graph_type == "bar":
            self.json["x"].append(newLabel)

    def addValue(self,newValue):
        if self.graph_type == "pie":
            self.json["values"].append(newValue)
        elif self.graph_type == "bar":
            self.json["y"].append(newValue)

    def getData(self):
        return flask.jsonify(JsonGraphBuilder.packJsonToArray(self.json))

    @staticmethod
    def packJsonToArray(json):
        data = []
        data.append(json)
        return data


    @staticmethod
    def getNewJsonTypePie():
            newjson = {}
            newjson["type"] = "pie"
            newjson["labels"] = []
            newjson["values"] = []
            return newjson

    @staticmethod
    def getNewJsonTypeBar():
        newjson = {}
        newjson["type"] = "bar"
        newjson["x"] = []
        newjson["y"] = []
        return newjson