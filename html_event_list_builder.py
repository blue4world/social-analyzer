import main as main
from db_manager import *
import flask

class HtmlEventListBuilder:
    def __init__(self,sql=None):
        self.html = None
        db = DbManager.getDataDb()
        cursor = db.execute(sql)
        self.generateHtml(cursor)

    def generateHtml(self, cursor):
        html = '<div class ="demo-charts mdl-color--white mdl-shadow--2dp mdl-cell mdl-cell--12-col mdl-grid" style="text-align: center;" >'
        html += """
        <style>main.get_db_data()
        .floated_img
        {
        float: left;
        }
        </style>"""
        #row = cursor.fetchone()
        #while row is not None:
        for row in cursor.fetchall():
            html += '<div class="floated_img"><a href="http://www.facebook.com/'+str(row[0])+'"><img src="http://graph.facebook.com/'+str(row[0])+'/picture" title="'+row[1]+'"height="50" width="50"></a></div>'
            #row = cursor.fetchone()
        html += '</div>'
        self.html = html


    def getData(self):
        return self.html

