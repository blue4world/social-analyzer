def queryMostVisitedPlaces(city, start_date, stop_date, limit):
    sql = """
    SELECT
    placename, count(user_id) as attendanceCount
    FROM
    (
    (SELECT
    places.place_id, places.placename
    from places
    """
    if city is not None and city != "All":
        sql = sql + ' where places.city LIKE "%' + city + '%"'
    sql = sql + """
    ) subPlaces
    LEFT
    JOIN
    (SELECT
    events.place_id, events.event_id
    from events """
    if start_date is not None:
        sql = sql + " where events.start_time > " + start_date
    if stop_date is not None and start_date is not None:
        sql = sql + " and events.start_time < " + stop_date
    elif stop_date is not None and start_date is None:
        sql = sql + " where events.start_time < " + stop_date
    sql = sql + """
    ) subEvents
    on
    subPlaces.place_id = subEvents.place_id
    ) sub
    LEFT
    JOIN
    attendance
    ON
    attendance.event_id = sub.event_id
    where attendance.type = 1 or attendance.type = 2
    group
    by
    sub.place_id
    order
    by
    attendanceCount
    desc
    """
    sql = sql + "LIMIT " + str(limit)
    return sql

def queryWordcloudEvents(city, start_date, stop_date,limit):
    sql = """
        SELECT DISTINCT
        sub.eventname
        FROM
        (
            (SELECT
        places.place_id
        from places
        """
    if city is not None and city != "All":
        sql = sql + ' where places.city LIKE "%' + city + '%"'
    sql = sql + """
        ) subPlaces
        LEFT
        JOIN
        (SELECT
        events.place_id, events.event_id, events.eventname
        from events
         """
    if start_date is not None:
        sql = sql + " where events.start_time > " + start_date
    if stop_date is not None and start_date is not None:
        sql = sql + " and events.start_time < " + stop_date
    elif stop_date is not None and start_date is None:
        sql = sql + " where events.start_time < " + stop_date
    sql = sql + """
        ) subEvents
        on
        subPlaces.place_id = subEvents.place_id
        ) sub
        """
    sql = sql + "LIMIT " + str(limit)
    return sql


def queryMostVisitedEvents(city, start_date, stop_date, limit):
    sql="""
    SELECT
    sub.eventname, count(user_id) as attendanceCount
    FROM
    (
        (SELECT
    places.place_id
    from places
    """
    if city is not None and city != "All":
        sql = sql + ' where places.city LIKE "%' + city + '%"'
    sql=sql+"""
    ) subPlaces
    LEFT
    JOIN
    (SELECT
    events.place_id, events.event_id, events.eventname
    from events
     """
    if start_date is not None:
        sql = sql + " where events.start_time > " + start_date
    if stop_date is not None and start_date is not None:
        sql = sql + " and events.start_time < " + stop_date
    elif stop_date is not None and start_date is None:
        sql = sql + " where events.start_time < " + stop_date
    sql=sql+"""
    ) subEvents
    on
    subPlaces.place_id = subEvents.place_id
    ) sub
    LEFT
    JOIN
    attendance
    ON
    attendance.event_id = sub.event_id
    where
    attendance.type = 1 or attendance.type = 2
    group
    by
    sub.event_id
    order
    by
    attendanceCount
    desc
    """
    sql = sql + "LIMIT " + str(limit)
    return sql


def queryEventsList(city, start_date, stop_date, limit):
    sql = """
    SELECT
    event_id, eventname
    from
    (
        (SELECT
    places.place_id
    from places
    """
    if city is not None and city != "All":
        sql = sql + ' where places.city LIKE "%' + city + '%"'
    sql = sql+"""
    ) subPlaces
    LEFT
    JOIN
    (SELECT
    event_id, eventname
    from events
    """
    if start_date is not None:
        sql = sql + " where events.start_time > " + start_date
    if stop_date is not None and start_date is not None:
        sql = sql + " and events.start_time < " + stop_date
    elif stop_date is not None and start_date is None:
        sql = sql + " where events.start_time < " + stop_date
    sql = sql + """
    order
    by
    start_time
    DESC) subEvents
    )
    """
    sql = sql + "LIMIT " + str(limit)
    return sql

def queryPeoplePalcesList(username):
    sql = """
    SELECT
    places.placename, places.city, count(subEvents.place_id) as visits
    from
    (SELECT
    subAttendance.event_id, events.place_id
    from
    (SELECT
    event_id
    from attendance where
    attendance.user_id = (SELECT
    users.user_id
    from users where
    users.user_name
    LIKE
    """
    sql += "%"+username+"%"
    sql += """
    )) subAttendance
    left
    join
    events
    on
    subAttendance.event_id = events.event_id
    ) subEvents
    left
    join
    places
    on
    subEvents.place_id = places.place_id
    group
    by
    subEvents.place_id
    """
    return sql


def queryHeatmap(city, start_date, stop_date):
    sql = """
    SELECT
    latitude, longitude, count(user_id) as count
    FROM
    (
    (SELECT
    places.place_id, places.placename, places.latitude, places.longitude
    from places
    """
    if city is not None and city != "All":
        sql = sql + ' where places.city LIKE "%' + city + '%"'
    sql = sql + """
    ) subPlaces
    LEFT
    JOIN
    (SELECT
    events.place_id, events.event_id
    from events
    """
    if start_date is not None:
        sql = sql + " where events.start_time > " + start_date
    if stop_date is not None and start_date is not None:
        sql = sql + " and events.start_time < " + stop_date
    elif stop_date is not None and start_date is None:
        sql = sql + " where events.start_time < " + stop_date
    sql = sql+"""
    ) subEvents
    on
    subPlaces.place_id = subEvents.place_id
    ) sub
    LEFT
    JOIN
    attendance
    ON
    attendance.event_id = sub.event_id
    where
    attendance.type = 1 or attendance.type = 2
    group
    by
    sub.place_id
    """
    return sql


'''
sql_city_list="""
        SELECT city,count(city) as occurence
        from places
        group by city
        order by occurence desc
        limit 10
"""

sql_most_visited_places_1 = """
        SELECT SUBSTR(placename, 0, 30),attendance_count from (
        SELECT
        events.start_time, events.end_time ,events.place_id, count(attendance.event_id) as attendance_count
        FROM
        attendance
        LEFT
        JOIN
        events
        on
        attendance.event_id = events.event_id
        where
        attendance.type = 1 or attendance.type = 2
        GROUP
        BY
        events.place_id
        ) sub
        LEFT JOIN places on places.place_id == sub.place_id
        """
sql_most_visited_places_2 = """
        order by sub.attendance_count desc LIMIT 20
        """
sql_most_visited_events_1 = """
        SELECT
        sub.eventname, sub.attendance_count
        from
        (
        SELECT
        events.start_time, events.end_time , events.place_id, events.eventname, count(attendance.event_id) as attendance_count
        FROM
        attendance
        LEFT
        JOIN
        events
        on
        attendance.event_id = events.event_id
        where
        attendance.type = 1 or attendance.type = 2
        group
        by
        events.event_id
        order
        by
        attendance_count
        desc
        ) sub
        LEFT
        JOIN
        places
        on
        sub.place_id = places.place_id
        """

sql_most_visited_events_2 = " limit 20"

sql_most_visited_events = """
        SELECT
        SUBSTR(events.eventname, 0, 30), count(attendance.event_id) as attendance_count
        FROM
        attendance
        LEFT
        JOIN
        events
        on
        attendance.event_id = events.event_id
        where
        attendance.type = 1 or attendance.type = 2
		group by
		events.event_id
		order by attendance_count desc
		limit 20
"""

"""
SELECT * from attendance left join events on events.event_id = attendance.event_id where attendance.user_id = (SELECT users.user_id from users where users.user_name LIKE "%Vojta Rocek%")

"""
'''

"""
List of events
SELECT * from (SELECT * from attendance where attendance.user_id = (SELECT users.user_id from users where users.user_name LIKE "%Mikulas Muron%")) subAttendance left join events on subAttendance.event_id = events.event_id
"""

"""
List of places by user

SELECT places.placename, places.city, count(subEvents.place_id) as visits from
(SELECT subAttendance.event_id, events.place_id from
(SELECT event_id from attendance where attendance.user_id = (SELECT users.user_id from users where users.user_name LIKE "%Mikulas Muron%")) subAttendance
left join events on subAttendance.event_id = events.event_id
) subEvents
left join places
on subEvents.place_id = places.place_id
on subEvents.place_id = places.place_id
group by subEvents.place_id
"""


"""
People with same events as I

SELECT users.user_name, subCommonList.user_id, subCommonList.common_count
from
(SELECT event_id,user_id, count(attendance.user_id) as common_count from
attendance
where
event_id
IN
(SELECT event_id from attendance
where attendance.user_id = (SELECT users.user_id from users where users.user_name = "Mikulas Muron")
)
and
attendance.type != 3
group by
attendance.user_id
order by common_count desc
limit 10) subCommonList
left join users
on subCommonList.user_id = users.user_id


"""

