import numpy as np
from db_manager import *
from PIL import Image
from os import path
import io
from flask import send_file
from wordcloud import WordCloud, STOPWORDS
from wordcloud_stop_words_list import *
from StringIO import *
from query_cache import *


class WordcloudBuilder:
    def __init__(self, sql=None):
        db = DbManager.getDataDb()
        cursor = db.execute(sql)
        self.sql = sql
        self.createImage(cursor)

    def createImage(self,cursor):
        names = ""
        for row in cursor.fetchall():
            name = row[0]
            if name is not None:
                names += name

        estonia_mask = np.array(Image.open(path.join("wordcloud_resources/estonia.png")))
        wordcloud = WordCloud(width=600, height=600, background_color="white", font_path="wordcloud_resources/roboto.ttf", max_words=500, mask=estonia_mask, stopwords=WordcloudStopWordsList.getWords()).generate(names)
        wordcloud.to_file(QueryCache.getFilePath(self.sql,extension=".png"))


    def getData(self):
        return send_file(QueryCache.getFilePath(self.sql,extension=".png"))




